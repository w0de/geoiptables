#!/usr/bin/env python
# -*- coding: utf-8 -*-

import subprocess
from setuptools import setup, find_packages

setup(
    url='https://bitbucket.org/w0de/geoiptables',
    name='geoiptables',
    author='w0de',
    version='0.6.6',
    description='''
    Lookup country/country IP block by IP;
lookup country IP block by country name. Database provided by db-ip.com.
ipv6 not supported: waiting on universal-ish iptables support.''',
    packages=find_packages(),
    install_requires=[
        'sh',
        'tqdm',
        'click',
        'funcy',
        'netaddr',
        'humanize',
        'dataset',
        'tomorrow',
        'pendulum',
        'pycountry',
        'pyipinfoio'
    ],
    entry_points={
        'console_scripts': [
            'geoiptables = geoiptables.cli:entry'
        ]
    },
    package_data={
        'geoiptables': [
            '*.db'
        ]
    }
)

subprocess.call('pip install python-iptables', shell=True)
