# -*- coding: utf-8 -*-

from functools import wraps

import click
from funcy import *
from netaddr import IPAddress, IPNetwork, IPRange, IPSet

import pendulum

def whisper(s):
    click.secho('  > ' + s.strip(), color='green', dim=True)

def msg(s, plain=False):
    if plain is False:
        click.secho('[+] ' + s.strip(), color='green', bold=True)
    else:
        click.echo(s.strip())
    click.echo('')

def err(s):
    click.secho('[!] ' + s.strip(), color='green', bold=True)
    click.echo('')

def exit(s=None):
    if s:
        err(s)
    ctx = click.get_current_context().find_root()
    ctx.exit(1)

def year_month(string=True):
    f = '%Y-%m'
    n = pendulum.now()
    if string is False:
        return (n.year, n.month)
    return n.format(f)

def ipsetize(value):

    def ize(val):
        if not isinstance(val, IPSet):
            if not isinstance(val, IPNetwork):
                if not isinstance(val, IPRange):
                    if not isinstance(val, (str, IPAddress)):
                        if isinstance(val, (list, tuple)):
                            try:
                                return ize(IPRange(*val))
                            except:
                                ipset = IPSet()
                                for v in val:
                                    ipset = ipset.union(ize(v))
                                return ipset
                        return ize(str(val))
                    return ize(IPNetwork(val))
                return ize(val.cidrs())
            return ize(IPSet([val]))
        return val

    if not isinstance(value, int):
        if not callable(value):
            return ize(value)
        return ipsetize(0)(value)
    index = value

    def decorator(func):

        @wraps(func)
        def wrapper(*args, **kwargs):
            try:
                args[index] = ize(args[index])
            except IndexError:
                pass
            finally:
                return func(*args, **kwargs)
        return wrapper

    return decorator

def walk_values_progressbar(func, collection, **pbar_kwargs):
    pbar = dict(width=0, show_pos=True, show_percent=True)
    _len = None
    if isinstance(collection, dict):
        if 'length' not in pbar_kwargs.keys():
            _len = lambda v: 1 if not is_list(v) else len(v)
            pbar['length'] = sum(_len(v) for v in collection.values())
        coll = collection.items()
    else:
        coll = collection

    pbar.update(pbar_kwargs)
    limit = pbar.pop('limit', False)
    err(str(pbar))
    data = dict()
    done = 0

    with click.progressbar(coll, **pbar) as bar:
        for key, value in bar:
            done += 1
            if key and value:
                try:
                    now_val = data.get(key, list())
                    for v in value:
                        now_val.append(func(key, v))
                    data[key] = now_val
                    if callable(_len):
                        bar.update(_len(value))
                except Exception as e:
                    err(str(key))
                    err(str(value))
                    #err(str(func(key, value)))
                    err(repr(e))
                    exit()
            if isinstance(limit, int):
                if done >= limit:
                    break
    return data
