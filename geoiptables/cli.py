# -*- coding: utf-8 -*-

from traceback import print_exc

from funcy import map, select_values, str_join

from humanize import intword as humanize

from . import database, isfile
from .iptables import IPTable
from .update import update
from .utils import click, err, msg, whisper

iptable = IPTable()

def cidrcat(ipset):
    return str_join(' ', map(str, list(ipset.iter_cidrs())))

def country_report(country, ipset, banned_set, output):
    click.echo('')
    whisper('name: ' + country.name)
    if output != 'piped':
        whisper('network: {} IPs'.format(humanize(ipset.size)))
        whisper('banned: {} IPs'.format(humanize(banned_set.size)))
        if output == 'very_verbose':
            attrs = str(country).replace('Country', '').strip('()').split(',')
            attrs = map(lambda a: a.strip(' \',').replace('=u\'', ': '), attrs)
            for a in reversed(attrs):
                if not a.startswith('name'):
                    whisper(a)
            whisper('accent: foreign')
    if output != 'verbose':
        click.echo('')
        whisper('network:')
        msg(cidrcat(ipset),  plain=True)
        click.echo('')
        whisper('banned:')
        msg(cidrcat(banned_set), plain=True)
    click.echo('')
    
@click.command()
@click.pass_context
@click.option('-c', '--cidrs', 'output', flag_value='piped',
              help='OUTPUT: just cidrs (for --lookup). Ideal for piping')
@click.option('-v', '--verbose', 'output', flag_value='verbose', default=True,
              help='OUTPUT: basic lookup/action info')
@click.option('-vv', '--very-verbose', 'output', flag_value='very_verbose',
              help='OUTPUT: more lookup/action info')
@click.option('-l', '--lookup', 'action', flag_value='lookup', default=True,
              help='ACTION: get country + ipblock by target ip/name')
@click.option('-b', '--ban', 'action', flag_value='ban',
              help='ACTION: get country + ipblock & ban all ips.')
@click.option('-u', '--unban', 'action', flag_value='unban',
              help='ACTION: get country + ipblock & unban any banned ips.')
@click.option('-s', '--status', 'action', flag_value='status',
              help='ACTION: list fully or partly banned countries + ipblocks')
@click.option('-up', '--update', 'action', flag_value='update',
              help='ACTION: update geoip database from db-ip.com (>= 15 min)')
@click.option('-d', '--debug', is_flag=True,
              help='Print raw tracebacks for exceptions')
@click.argument('target', nargs=1, required=False,
                metavar='[ipv4 | cidr | country:iso2/3 | country:name]')
def run(context, output, action, debug, target):

    def _run():
        try:
            for country, ipset in database.get(target):
                banned_set = ipset.intersection(iptable.banned_set)
                if action in ('lookup', 'status'):
                    country_report(country, ipset, banned_set, output)
                else:
                    if action == 'ban' and ipset != banned_set:
                        iptable.ban(ipset)
                    elif action == 'unban' and bool(banned_set) is True:
                        iptable.unban(ipset)
                    else:
                        context.exit('No new {}s'.format(action))
                    country_report(country,
                                   ipset,
                                   ipset.intersection(iptable.banned_set),
                                   output)
        except Exception as e:
            if debug is True:
                print_exc()
                context.exit(1)
            elif isinstance(e, LookupError):
                if target:
                    err('Lookup failed for {}'.format(str(target)))
                    err('Is it an IP or a common country name/code?')
                elif action == 'status':
                    banned = database.reverse_lookup(iptable.banned_set)
                    if output != 'piped':
                        msg('{} countries partially or fully banned'.format(
                            str(len(banned.keys()))))
                    if banned:
                        full = select_values(banned_set.issuperset, banned)
                        part = select_values(banned_set.issubset, banned)
                        msg('Complete bans:')
                        if output == 'piped':
                            msg(str_join(', ', full.keys()))
                        else:
                            for c, s in full.items():
                                country_report(c, s, banned_set, output)
                        msg('Partial bans:')
                        if output == 'piped':
                            msg(str_join(', ', part.keys()))
                        else:
                            for c, s in full.items():
                                country_report(c, s, banned_set, output)
                    context.exit()
                err('Action "{}" requires a valid target'.format(action))
                err(context.get_usage())
            else:
                err('Unexpected exception: ' + repr(e))
                context.exit(1)

    if action == 'update':
        csv = None
        if target:
            csv = target if isfile(target) else csv
        update(cli='debug' if debug else True, csvfile=csv)
    else:
        _run()

    context.exit()

def entry(argv=None):
    run(argv)
