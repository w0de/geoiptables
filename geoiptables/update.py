import csv
import gzip
import sys
from time import sleep

from funcy import *
from netaddr import IPSet, cidr_merge, iprange_to_cidrs, valid_ipv4
from pycountry import countries
from tqdm import tqdm

from sqlalchemy.types import PickleType, String
from tomorrow import threads

from . import DBPATH, DIRPATH, database, isfile, join
from .utils import (click, err, exit, ipsetize, msg, walk_values_progressbar,
                    year_month)


def uri():
    return 'http://download.db-ip.com/free/dbip-country-{}.csv.gz'.format(
        year_month(string=True)
    )

def download(cli):
    path = join(DIRPATH, 'db-ip.csv.gz.tmp')
    from sh import wget
    if cli:
        msg('Downloading the geoip database... (thanks db-ip.com!)')
        wget = wget.bake('--show-progress')
    wget('-nv', uri(), '-O', path, _fg=True)
    return path

def _prep(a2, cidrs):
    return map(lambda c: dict(country=a2, network=str(c)), cidrs)

def _net(a2, fip, lip):
    return _prep(a2, iprange_to_cidrs(fip, lip))

def _cull(row):
    try:
        first_ip, last_ip, alpha_2 = map(lambda r: r.strip().lower(), row)
        assert alpha_2 != 'zz'
        assert False not in map(valid_ipv4, (first_ip, last_ip))
        return _net(alpha_2.upper(), first_ip, last_ip)
    except (AssertionError, ValueError):
        return []

def collect(rows, func):
    f = threads(min(50, int(len(rows)/10000)))(func)
    for r in rows:
        yield f(r)

def update(cli=False, csvfile=None):
    path = csvfile if csvfile else download(cli)
    with gzip.open(path, 'r') as f:
        reader = csv.reader(f)
        #count = 5000
        #if cli != 'debug':
        _rows = [r for r in reader]
    rows = list()
    pf = lambda r: '' if not r else r[-1]
    for result in tqdm(collect(_rows, _cull), desc='Parsing ipdb',
                        dynamic_ncols=True, total=len(_rows), postfix=pf(rows)):
        rows = rows + list(result)

    msg('Saving final geoip database...')
    db = database.get_db(create=True)
    networks = db.create_table('networks')
    ranges = db.create_table('ranges')
    networks.insert_many(rows)
    db.commit()
    if csvfile is None:
        from os import remove
        remove(path)
