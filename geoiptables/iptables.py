# -*- coding: utf-8 -*-

import iptc
from funcy import map

from .utils import IPNetwork, IPSet, ipsetize


def new_rule(cidr):
    rule = iptc.Rule()
    rule.protocol = 'tcp'
    rule.dst = '0.0.0.0/0.0.0.0'
    rule.src = str(IPNetwork(cidr))
    rule.create_match("tcp")
    rule.create_target("DROP")
    return rule

@ipsetize
def intersecting(ipset, **cmpsets):
    return select_values(
        lambda cmp: bool(ipsetize(cmp).intersection(ipset)), cmpsets)

class IPTable(object):
    _banned_set = None

    def __init__(self):
        self.refresh()

    def refresh(self):
        if hasattr(self, 't'):
            self.t.commit()
        self._banned_set = None
        self.t = iptc.Table(iptc.Table.FILTER)
        self.c = iptc.Chain(self.t, 'INPUT')
        rules = filter(lambda r: (r.target.name,
                                  r.protocol,
                                  r.dst) == ('DROP',
                                             'tcp',
                                             '0.0.0.0/0.0.0.0'), self.c.rules)
        self.r = {rule: IPNetwork(rule.src) for rule in rules}

    @property
    def banned_set(self):
        if self._banned_set is None:
            self._banned_set = IPSet(self.r.values())
        return self._banned_set

    @ipsetize(1)
    def unban(self, ipset):
        acted = int(ipset.difference(self.banned_set).size)
        for rule, rule_ipnet in intersecting(ipset, **self.r):
            still_banned_ipset = IPSet([rule_ipnet]).difference(ipset)
            for ipnet in still_banned_ipset.iter_cidrs():
                rule = new_rule(ipnet)
                self.c.append_rule(rule)
            self.c.delete_rule(rule)
        if bool(acted) is True:
            self.refresh()
        return acted

    @ipsetize(1)
    def ban(self, ipset):
        acted = IPSet()
        not_banned = ipsetize(ipset.difference(self.banned_set))
        for ipnet in not_banned.iter_cidrs():
            rule = new_rule(ipnet)
            self.c.append_rule(rule)
            acted.add(ipnet)
        if bool(acted) is True:
            self.refresh()
        return acted
