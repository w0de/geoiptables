# -*- coding: utf-8 -*-

import sqlite3

from funcy import *
from netaddr import AddrFormatError, IPAddress, IPNetwork, IPSet, iprange_to_cidrs
from pycountry import countries

import dataset
from pyipinfoio import IPLookup, IPLookupError
from sqlalchemy.exc import OperationalError

from . import DBPATH, isfile
from .utils import ipsetize

__DB = None
__INVALID_IPINFO_IO = ('EU')

def get_db(create=False):
    if not __DB:
        global __DB
        __DB = dataset.connect('sqlite:///' + DBPATH)
    if create is True:
        __DB.get_table('ranges').drop()
        __DB.get_table('networks').drop()
        global __DB
        __DB = None
        return get_db(create=False)
    return __DB

@memoize
def query(k, cli=False):
    db = get_db()
    ntable = db.get_table('networks')
    network = ntable.find(country=k)
    if not network:
        raise LookupError(k)
    print 'ipsetize...'
    return IPSet(map(lambda n: IPNetwork(n['network']), network))

def get(value, types=None, cli=False):

    @memoize
    def geo(v):
        print 'geo...'
        c = countries.lookup(v)
        k = c.alpha_2.strip().upper()
        s = query(k, cli=cli)
        return [[c, s]]

    def _ip_from_database(v):
        print 'ip from database...'
        i = IPAddress(v)
        d = get_db()
        t = d.get_table('networks')
        for row in t.all():
            if i in query(row['country']):
                return geo(row['country'])
        raise LookupError(v)

    @memoize
    def ip(v):
        print 'ip...'
        r = IPLookup().lookup(str(v), 'country')
        if not r or r in __INVALID_IPINFO_IO:
            return _ip_from_database(v)
        return geo(r)

    def net(v):
        print 'net...'
        r = []
        s = IPSet(IPNetwork(v))
        while s:
            for n in s.iter_cidrs():
                print str(n.first)
                _r = ip(n.first)[0]
                print str
                r.append(_r)
                break
            s = s.difference(_r[1])
        return r

    vfuncs = [ip, geo, net]
    vtypes = zipdict(
        map(lambda v: v.__name__, vfuncs),
        map(lambda v: (partial(v, value), (AddrFormatError,
                                           IPLookupError,
                                           LookupError,
                                           ValueError)
                       ), vfuncs)
        )

    _types = vtypes.keys() if not types else [types] if not is_iter(types) \
             else types

    try:
        tests = [vtypes[t] for t in _types]
    except KeyError:
        bad = filter(lambda t: t not in vtypes.keys(), _types)
        raise ValueError(bad, vtypes.keys())

    tests.append(raiser(LookupError, value))
    rtr = fallback(*tests)
    return rtr if not callable(rtr) else rtr()
