# -*- coding: utf-8 -*-

from funcy import first, filter
from os.path import join, isfile, abspath, dirname

DIRPATH = abspath(dirname(__file__))
DBPATH = join(DIRPATH, 'geoip.db')
